<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace MailBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Component\HttpFoundation\Request;

/**
 * Description of PersonneController
 *
 * @author Maria
 */
class PersonneController extends Controller {

    /**
     * @Route("/liste", name="liste_user")
     */
    public function exempleListeAction(Request $request) {
        $em = $this->getDoctrine()->getManager();
        $personnes = $em->getRepository('MailBundle:Personne')->findAll();

        return $this->render('MailBundle:Message:liste.html.twig', array(
                    'listePers' => $personnes,
        ));
    }

    /**
     * @Route("/ajouter-user", name="ajouter_user")
     */
    public function ajouterUserAction() {
        $em = $this->getDoctrine()->getManager();
        $personneHandler = new PersonneHandler();
        return $this->render('MailBundle:Message:ajouterUser.html.twig');
    }

    /**
     * @Route("/modifier-user", name="modifier_user")
     */
    public function modifierUserAction() {
        return $this->render('MailBundle:Message:modifierUser.html.twig');
    }

}
