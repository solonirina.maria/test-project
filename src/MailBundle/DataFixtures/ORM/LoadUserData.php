<?php

namespace MailBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use MailBundle\Entity\Message;

class LoadGroupData extends AbstractFixture implements OrderedFixtureInterface
{
    public function load(ObjectManager $manager)
    {
        for ($i = 0 ;$i <= 200; $i++){
            $message = new Message();
            $message->setObject('message portant le numero:'.$i);
            $message->setContent('content');
            $message->setReceivedDate(new \Datetime());
            $message->setState(false);
            $message->setSender('sender');

            $manager->persist($message);
        }
        
        $manager->flush();
    }

    public function getOrder()
    {
        // the order in which fixtures will be loaded
        // the lower the number, the sooner that this fixture is loaded
        return 1;
    }
}